'''
Any function that takes has a param named colors expects colors to be
a 4-or 5-element list (or tuple) of color names (as strings) drawn from the following:
black
brown
red
orange
yellow
green
blue
violet
grey
white
gold
silver
'''

def decodeResistance(colors):
    return {
        'value': getBaseResistanceValue(colors),
        'units': getUnits(colors),
        'tolerance' : decodeTolerance(colors),
        'formatted' : createFormattedResistanceString(colors)
    }

def createFormattedResistanceString(colors):
    units = getUnits(colors)
    tolerance = decodeTolerance(colors)
    value = getBaseResistanceValue(colors)
    return str(value) + ' ' + units +  ' +/- ' + str(tolerance) + '%'

def getBaseResistanceValue(colors):
    digits = decodeSignificantFigures(colors)
    multiplier = decodeMultiplier(colors)
    return digits * multiplier[0]

def getUnits(colors):
    multiplier = decodeMultiplier(colors)
    return multiplier[1] + "ohms"

def decodeColorDigit(color):
    normalizedColor = color.lower()
    try:
        result = {
            'black' : '0',
            'brown' : '1',
            'red'   : '2',
            'orange': '3',
            'yellow': '4',
            'green' : '5',
            'blue'  : '6',
            'violet': '7',
            'grey'  : '8',
            'white' : '9'
        }[normalizedColor]
        return result
    except(KeyError):
        raise ValueError("invalid color")

def validateColorsList(colors):
    if not len(colors) in [4, 5]:
        raise ValueError("invalid number of colors")

    validDigitColors = ['black', 'brown', 'red', 'orange', 'yellow', 'green',
        'blue', 'violet', 'grey', 'white']
    validMultiplierColors = validDigitColors + ['gold', 'silver']
    validToleranceColors = ['brown', 'red', 'green', 'blue', 'violet', 'grey', 'gold', 'silver']

    if not colors[0] in validDigitColors:
        raise ValueError("invalid band 1: " + colors[0])

    if not colors[1] in validDigitColors:
        raise ValueError("invalid band 2: " + colors[1])

    if not colors[2] in validDigitColors:
        raise ValueError("invalid band 3: " + colors[2])

    if not colors[3] in validMultiplierColors:
        raise ValueError("invalid band 4: " + colors[4])

    if len(colors) == 5:
        if not colors[4] in validToleranceColors:
            raise ValueError("invalid band 5: " + colors[4])

def decodeSignificantFigures(colors):
    significantDigits = decodeColorDigit(colors[0])
    significantDigits += decodeColorDigit(colors[1])
    significantDigits += decodeColorDigit(colors[2])
    return int(significantDigits)

def decodeMultiplier(colors):
    multipliers = {
        'black' : (1, ''),
        'brown' : (10, ''),
        'red'   : (100, ''),
        'orange': (1, 'K'),
        'yellow': (10, 'K'),
        'green' : (100, 'K'),
        'blue'  : (1, 'M'),
        'violet': (10, 'M'),
        'grey'  : (100, 'M'),
        'white' : (1, 'G'),
        'gold'  : (0.1, ''),
        'silver': (0.01, '')
    }
    return multipliers[colors[3].lower()]

def decodeTolerance(colors):
    # the 'no tolerance band' case
    if len(colors) == 4:
        return 20

    tolerances = {
        'brown'  : 1,
        'red'    : 2,
        'green'  : 0.5,
        'blue'   : 0.25,
        'violet' : 0.1,
        'grey'   : 0.05,
        'gold'   : 5,
        'silver' : 10
    }
    return tolerances[colors[4].lower()]
