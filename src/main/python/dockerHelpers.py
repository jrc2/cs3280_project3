import os
import subprocess


def setUpDocker(env, port):
    subprocess.run(f"docker build --tag {env}_image .", shell=True)
    subprocess.run(f"docker run -d -p {port}:8000 -v {os.path.join(str(os.getcwd()), 'target', 'dist', 'rcc')}:{os.sep}app --name {env}_container {env}_image", shell=True)


def tearDownDocker(env):
    subprocess.run(f"docker stop {env}_container", shell=True)
    subprocess.run(f"docker rm {env}_container", shell=True)
    subprocess.run(f"docker image rm {env}_image", shell=True)
