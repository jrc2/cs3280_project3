import http.server
from urllib.parse import parse_qs
import json
from resistor import decodeResistance


class ResistorCalculatorHandler(http.server.BaseHTTPRequestHandler):

    '''
    Expects a resource of the form:

    /decode?band1=color&band2=color&band3=color&band4=color

    or

    /decode?band1=color&band2=color&band3=color&band4=color&band5=color

    where color is one of: black, brown, red, orange, yellow, green,
    blue, violet, grey, white, gold, silver

    Response will be a JSON object of the form:

    {
        "value": numeric base resistance,
        "units": units value (ohms, Kohms, Mohms, or Gohms),
        "tolerance": tolerance as a percentage e.g. "20%",
        "formatted": nicely-formatted string of the entire resistance
    }
    '''
    def do_GET(self):
        self.log_message("path: %s", self.path)
        try:
            [resource, queryString] = self.path.split('?')
            self.log_message("resource: %s", resource)
            self.log_message("query string: %s", queryString)
            if resource != "/decode":
                self.log_message("resource: " + resource)
                self.send_error(404)
            bands = parse_qs(queryString)
            self.log_message("built bands... qs = %s", str(bands))
            bandsList = self.makeBandsList(bands)
            self.log_message("built bandsList...")
            decoded = decodeResistance(bandsList)
            body = self.buildResponseBody(decoded)
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.end_headers()
            self.wfile.write(bytes(body, 'UTF-8'))
        except Exception as e:
            self.send_error(400, str(e))

    def makeBandsList(self, bandsDict):
        if len(bandsDict) == 4:
            return [
                bandsDict['band1'][0],
                bandsDict['band2'][0],
                bandsDict['band3'][0],
                bandsDict['band4'][0]
            ]
        elif len(bandsDict) == 5:
            return [
                bandsDict['band1'][0],
                bandsDict['band2'][0],
                bandsDict['band3'][0],
                bandsDict['band4'][0],
                bandsDict['band5'][0]
            ]
        else:
            raise Exception()

    def buildResponseBody(self, decodedJson):
        resistance = decodedJson['formatted']
        return f"""
        <html>
            <head>
                <title>Decoded Resistance</title>
            </head>
            <body>
                <h3 id='resistance'>{resistance}</h3>
            </body>
        </html>
        """

# start the server
print("Resistance-decoding webserver running")
daemon = http.server.HTTPServer(('', 8000), ResistorCalculatorHandler)
daemon.serve_forever()
