import unittest
from resistor import decodeTolerance

class DecodeToleranceTest(unittest.TestCase):

    def test_toleranceForFourBandResistorShouldBe20(self):
        colors = ['red', 'red', 'red', 'red']
        self.assertEquals(20, decodeTolerance(colors))

    def test_shouldDecodeFiveBandTolerance(self):
        colors = ['red', 'red', 'red', 'red', 'silver']
        self.assertEquals(10, decodeTolerance(colors))
