import unittest
from resistor import decodeColorDigit

class DecodeColorDigitTests(unittest.TestCase):

    def test_shouldAcceptValidLowercaseColor(self):
        self.assertEqual('4', decodeColorDigit('yellow'))

    def test_shouldAcceptValidUppercaseColor(self):
        self.assertEqual('4', decodeColorDigit('YELLOW'))

    def test_shouldNotAcceptInvalidColor(self):
        with self.assertRaises(ValueError):
            decodeColorDigit('chartreuse')
