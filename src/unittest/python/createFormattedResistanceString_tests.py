import unittest
from resistor import createFormattedResistanceString

class CreateFormattedResistanceStringTests(unittest.TestCase):

    def test_shouldFormatResistanceWithImplicitTolerance(self):
        colors = ['brown', 'red', 'orange', 'black']
        expected = "123 ohms +/- 20%"
        self.assertEquals(expected, createFormattedResistanceString(colors))

    def test_shouldFormatResistanceWithExplicitTolerance(self):
        colors = ['brown', 'red', 'orange', 'black', 'green']
        expected = "123 ohms +/- 0.5%"
        self.assertEquals(expected, createFormattedResistanceString(colors))

    def test_shouldFormatResistanceWithBaseMultiplier(self):
        colors = ['brown', 'red', 'orange', 'red', 'green']
        expected = "12300 ohms +/- 0.5%"
        self.assertEquals(expected, createFormattedResistanceString(colors))

    def test_shouldFormatResistanceWithMultiplierThatIncludesPrefix(self):
        colors = ['brown', 'red', 'orange', 'violet', 'green']
        expected = "1230 Mohms +/- 0.5%"
        self.assertEquals(expected, createFormattedResistanceString(colors))
