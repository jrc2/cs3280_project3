import unittest
from resistor import validateColorsList

class ValidateColorsListTests(unittest.TestCase):

    def setUp(self):
        self.validDigitColors = ['black', 'brown', 'red', 'orange', 'yellow', 'green',
            'blue', 'violet', 'grey', 'white']
        self.validMultiplierColors = self.validDigitColors + ['gold', 'silver']
        self.validToleranceColors = ['brown', 'red', 'green', 'blue', 'violet', 'grey', 'gold', 'silver']

    def test_shouldNotAcceptInvalidFirstBand(self):
        with self.assertRaises(ValueError):
            validateColorsList(['chartreuse', 'red', 'red', 'red', 'red'])

    def test_shouldNotAcceptInvalidSecondBand(self):
        with self.assertRaises(ValueError):
            validateColorsList(['red', 'chartreuse', 'red', 'red', 'red'])

    def test_shouldNotAcceptInvalidThirdBand(self):
        with self.assertRaises(ValueError):
            validateColorsList(['red', 'red', 'chartreuse', 'red', 'red'])

    def test_shouldNotAcceptInvalidFourthBand(self):
        with self.assertRaises(ValueError):
            validateColorsList(['red', 'red', 'red', 'chartreuse', 'red', 'red'])

    def test_shouldNotAcceptInvalidFifthBand(self):
        with self.assertRaises(ValueError):
            validateColorsList(['red', 'red', 'red', 'red', 'chartreuse', 'red'])

    def test_shouldNotAcceptThreeBandList(self):
        with self.assertRaises(ValueError):
            validateColorsList(['red', 'red', 'red'])

    def test_shouldNotAcceptSixBandList(self):
        with self.assertRaises(ValueError):
            validateColorsList(['red', 'red', 'red', 'red', 'red', 'red'])

    def test_shouldAcceptAllValidFiveBandCombinations(self):
        for band1 in self.validDigitColors:
            for band2 in self.validDigitColors:
                for band3 in self.validDigitColors:
                    for band4 in self.validMultiplierColors:
                        for band5 in self.validToleranceColors:
                            validateColorsList([band1, band2, band3, band4, band5])

    def test_shouldAcceptAllValidFourBandCombinations(self):
        for band1 in self.validDigitColors:
            for band2 in self.validDigitColors:
                for band3 in self.validDigitColors:
                    for band4 in self.validMultiplierColors:
                        validateColorsList([band1, band2, band3, band4])

    def test_shouldNotAllowInvalidToleranceColors(self):
        for tol in ['black', 'orange', 'yellow', 'white']:
            with self.assertRaises(ValueError):
                validateColorsList(['red', 'red', 'red', 'red', tol ])
