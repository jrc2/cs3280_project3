import unittest
from resistor import decodeMultiplier

class DecodeMultiplierTests(unittest.TestCase):

    def test_shouldDecodeMultiplierWithoutUnit(self):
        colors = [ 'red', 'red', 'red', 'brown', 'red']
        self.assertEqual((10, ''), decodeMultiplier(colors))

    def test_shouldDecodeMultiplierWithUnit(self):
        colors = [ 'red', 'red', 'red', 'green', 'red']
        self.assertEqual((100, 'K'), decodeMultiplier(colors))
