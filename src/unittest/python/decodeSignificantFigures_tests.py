import unittest
from resistor import decodeSignificantFigures

class DecodeSignificantFiguresTests(unittest.TestCase):

    def test_shouldDecodeThreeDifferentDigits(self):
        colors = [ 'brown', 'red', 'orange' ]
        self.assertEqual(123, decodeSignificantFigures(colors))

    def test_shouldDecodeThreeSameDigits(self):
        colors = [ 'red', 'red', 'red' ]
        self.assertEqual(222, decodeSignificantFigures(colors))
