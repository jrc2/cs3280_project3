import unittest
from resistor import decodeResistance

class DecodeResistanceTests(unittest.TestCase):

    def test_shouldDecodeResistanceWithImplicitTolerance(self):
        colors = ['brown', 'red', 'orange', 'black']
        expected = {
            'value' : 123,
            'units' : "ohms",
            'tolerance' : 20,
            'formatted' : "123 ohms +/- 20%"
        }
        self.assertEquals(expected, decodeResistance(colors))

    def test_shouldDecodeResistanceWithExplicitTolerance(self):
        colors = ['brown', 'red', 'orange', 'black', 'green']
        expected = {
            'value' : 123,
            'units' : "ohms",
            'tolerance' : 0.5,
            'formatted' : "123 ohms +/- 0.5%"
        }
        self.assertEquals(expected, decodeResistance(colors))

    def test_shouldDecodeResistanceWithBaseMultiplier(self):
        colors = ['brown', 'red', 'orange', 'red', 'green']
        expected = {
            'value' : 12300,
            'units' : "ohms",
            'tolerance' : 0.5,
            'formatted' : "12300 ohms +/- 0.5%"
        }
        self.assertEquals(expected, decodeResistance(colors))

    def test_shouldDecodeResistanceWithMultiplierThatIncludesPrefix(self):
        colors = ['brown', 'red', 'orange', 'violet', 'green']
        expected = "1230 Mohms +/- 0.5%"
        expected = {
            'value' : 1230,
            'units' : "Mohms",
            'tolerance' : 0.5,
            'formatted' : "1230 Mohms +/- 0.5%"
        }
        self.assertEquals(expected, decodeResistance(colors))
