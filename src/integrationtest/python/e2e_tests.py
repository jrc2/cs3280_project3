import unittest
from selenium import webdriver
import os
import subprocess
import dockerHelpers


def setUpModule():
    dockerHelpers.setUpDocker('testenv', 8000)


def tearDownModule():
    dockerHelpers.tearDownDocker('testenv')


class SimpleE2ETest(unittest.TestCase):

    def setUp(self):
        ffoptions = webdriver.firefox.options.Options()
        ffoptions.headless = True
        self.driver = webdriver.Firefox(options=ffoptions)

    def tearDown(self):
        self.driver.close()

    def testShouldAlwaysPass(self):
        self.driver.get("http://127.0.0.1:8000/decode?band1=blue&band2=blue&band3=blue&band4=blue")
        title = self.driver.title
        self.assertEqual('Decoded Resistance', title)


class NegativeTests(unittest.TestCase):

    def setUp(self):
        ffoptions = webdriver.firefox.options.Options()
        ffoptions.headless = True
        self.driver = webdriver.Firefox(options=ffoptions)

    def tearDown(self):
        self.driver.close()

    def testRootUrl(self):
        self.driver.get("http://127.0.0.1:8000")
        firstP = self.driver.find_element_by_tag_name('p')
        self.assertEquals('Error code: 400', firstP.text)

    def testNoDecodeBands(self):
        self.driver.get("http://127.0.0.1:8000/decode")
        firstP = self.driver.find_element_by_tag_name('p')
        self.assertEquals('Error code: 400', firstP.text)

    def testDifferentResource(self):
        self.driver.get("http://127.0.0.1:8000/nothere?band1=blue&band2=red&band3=orange&band4=violet")
        firstP = self.driver.find_element_by_tag_name('p')
        self.assertEquals('Error code: 404', firstP.text)

    def testThreeBands(self):
        self.driver.get("http://127.0.0.1:9000/decode?band1=blue&band2=blue&band3")
        firstP = self.driver.find_element_by_tag_name('p')
        self.assertEquals('Error code: 400', firstP.text)


class PositiveTests(unittest.TestCase):

    def setUp(self):
        ffoptions = webdriver.firefox.options.Options()
        ffoptions.headless = True
        self.driver = webdriver.Firefox(options=ffoptions)

    def tearDown(self):
        self.driver.close()

    def testFourValidBands(self):
        self.driver.get("http://127.0.0.1:9000/decode?band1=blue&band2=blue&band3=blue&band4=blue")
        resistance = self.driver.find_element_by_id('resistance')
        self.assertEquals('666 Mohms +/- 20%', resistance.text)

    def testFiveValidBands(self):
        self.driver.get("http://127.0.0.1:8000/decode?band1=blue&band2=blue&band3=blue&band4=blue&band5=violet")
        resistance = self.driver.find_element_by_id('resistance')
        self.assertEquals('666 Mohms +/- 0.1%', resistance.text)


if __name__ == '__main__':
    unittest.main()
