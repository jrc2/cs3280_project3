from pybuilder.core import use_plugin
from pybuilder.core import init
from pybuilder.core import Author
from pybuilder.core import task, depends
from pybuilder.core import use_bldsup
import os
import subprocess


# plugins for common tasks
use_plugin('python.core')
use_plugin('python.unittest')
use_plugin('python.install_dependencies')
use_plugin('python.distutils')
use_plugin('python.integrationtest')
use_bldsup(build_support_dir=os.path.join('src', 'main', 'python'))
import dockerHelpers

# the task run when pyb is executed
default_task = 'publish'

@init
def config_integrationtest(project):
    project.set_property('integrationtest_inherit_environment', True)

# target directory (relative)
target_dir = os.path.join('$dir_target', 'dist', 'rcc')

@init
def normalize_target_dir(project):
    project.set_property('dir_dist', target_dir)

@init
def initialize(project):
    # project information
    project.name = 'Resistor Color Code Web Service'
    project.version = '1.0'
    project.summary = 'Project for CS3280'
    project.description = '''Given a set of band colors, returns a json object with the type of resistor'''
    project.authors = [Author('Lewis Baumstark', 'lewisb@westga.edu')]
    project.license = "not for redistribution"
    project.url = 'https://www.westga.edu/academics/cosm/computer-science/'
    project.depends_on('selenium')

@task
def deploy():
    dockerHelpers.tearDownDocker('deployenv')
    dockerHelpers.setUpDocker('deployenv', 9000)
